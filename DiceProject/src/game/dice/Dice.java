package game.dice;

import java.util.Random;

public class Dice {
    private int off_set;

    private int[] dice = new int[100];
    
    
    public int getOff_set() {
        return off_set;
    }
    public void setOff_set(int off_set) {
        this.off_set = off_set;
    }
    public int[] getDice() {
        return dice;
    }
    public void setDice(int[] dice) {
        this.dice = dice;
    }


    public Dice(int off_set) {
        this.off_set = off_set;
    }
    public Dice() {
    }


    public int rollDice(){
        Random random = new Random();
        int num = random.nextInt(100);
        int cnt, index=0;
        for (int i = 1; i <= 6; i++){       
            if (i == off_set)
                cnt = 20;
            else 
                cnt = 16;
            while (cnt > 1){
                dice[index] = i;
                cnt--;
                index++;
            }
        }
        return this.getDice()[num];
    }
    
    
}
