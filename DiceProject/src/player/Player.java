package player;

import game.dice.Dice;

public class Player {
    private String name;
    private int pt = 0;
    private int win=0;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getPt() {
        return pt;
    }
    public void setPt(int pt) {
        this.pt = pt;
    }
    public int getWin() {
        return win;
    }
    public void setWin(int win) {
        this.win = win;
    }

    
    public Player(String name) {
        this.name = name;
    }
    public Player() {
    }


    public void play(Dice d){
        int point = d.rollDice();
        System.out.printf("| %s've got  %d |\n",this.getName() , point);
        this.pt += point;
    }
    public void display(){
        if (this.win == 0)
            System.out.printf("| %-10s | %-4d | %8s |\n", this.getName(), this.getPt(), "PLAYING");
        else if (this.win == -1)
            System.out.printf("| %-10s | %-4d | %8s |\n", this.getName(), this.getPt(), "LOST");
    }

    public void checkLose(){
        if (this.getPt()>21){
            System.out.printf("| %s lost | Point: %d |\n", this.getName(), this.getPt());
            this.win = -1;
        }
        else if (this.getPt() == 21){
            System.out.printf("| %s won | Point: %d |\n", this.getName(), this.getPt());
            this.win = 1;
        }
        else
            return;
    }
    
    
    

}
