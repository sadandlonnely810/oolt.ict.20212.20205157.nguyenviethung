package player;

public class NPC extends Player { 
    private static int id=1;

    public NPC() {
        this.setName("Bot "+Integer.toString(NPC.id));
        NPC.id++;
    }

    public void expressLost(){
        System.out.println(this.getName()+" is defeated ");
    }
}
