package player;

import game.dice.Dice;

public class Master {

    public Master() {
    }
    
    public void assignPlayer(Player p, Dice d){
        System.out.println("It's turn of player "+p.getName());
        p.play(d);
    }

    public void assignPlayer(NPC p, Dice d){
        System.out.println("It's turn of NPC "+p.getName());
        p.play(d);
    }

    public void check(Player p){
        p.checkLose();
    }

    public void check(NPC p){
        p.checkLose();
    }
}
