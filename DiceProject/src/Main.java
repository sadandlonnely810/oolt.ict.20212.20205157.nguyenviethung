import java.util.Random;
import java.util.Scanner;

import game.dice.Dice;
import player.NPC;
import player.Player;

public class Main {-
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int numberOfPlayer, choice;
        int status=0;
        int turn = 1;
        Dice[] d = new Dice[4];
        for (int i=0; i<4; i++){
            d[i] = new Dice(i);
        }

        System.out.println("Enter the number of player:");
        numberOfPlayer = scanner.nextInt();
        scanner.nextLine();

        Player[] players = new Player[numberOfPlayer];
        NPC[] bots = new NPC[4-numberOfPlayer];
        String name;
        for (int i=0; i<numberOfPlayer; i++){
            System.out.printf("Enter the name of player %d: ", i+1);
            name = scanner.nextLine();
            players[i] = new Player(name);
            status=Math.max(status, players[i].getWin());
        }
        for (int i = 0; i<4-numberOfPlayer; i++){
            System.out.printf("Create bot %d successfully\n", i+1);
            bots[i] = new NPC();
            status=Math.max(status, bots[i].getWin());
        }



        while (status<1){
            System.out.printf("+----------+\n");
            System.out.printf("| TURN: %d |\n", turn);
            System.out.printf("+----------+\n\n");
            turn++;
            System.out.printf("+-------------------------------+\n");
            System.out.printf("| 1. Assign next player         |\n");
            System.out.printf("| 2. Display all players status |\n");
            System.out.printf("+-------------------------------+\n");
            System.out.printf(" Enter your choice: ");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    assign(players, bots, d, numberOfPlayer);
                    break;
                case 2:
                    showAllPlayer(players, bots, numberOfPlayer);
                    break;
                default:
                    break;
            }


            for (int i=0; i<numberOfPlayer; i++)
                status=Math.max(status, players[i].getWin());           
            for (int i = 0; i<4-numberOfPlayer; i++)
                status=Math.max(status, bots[i].getWin());            
        }

        
        
    }

    private static void showAllPlayer(Player[] players, NPC[] bots, int numberOfPlayer){
        System.out.printf("+------------------------------+\n");
        for (int i=0; i<numberOfPlayer; i++)
            if (players[i].getWin()<1)
                players[i].display();
        for (int i=0; i<4-numberOfPlayer; i++)
            if (bots[i].getWin()<1)
                bots[i].display();
        System.out.printf("+------------------------------+\n");
    }

    private static void assign(Player[] players, NPC[] bots, Dice[] d, int numberOfPlayer){
        String name;
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.printf("+------------+\n");
        for (int i=0; i<numberOfPlayer; i++)
            if (players[i].getWin()==0)
                System.out.printf("| %10s |\n", players[i].getName());
        for (int i=0; i<4-numberOfPlayer; i++)
            if (bots[i].getWin()==0)
                System.out.printf("| %10s |\n", bots[i].getName());
            System.out.printf("+------------+\n");
        
        System.out.printf(" Choose the player's name you want for playing this turn: ");
        name = scanner.nextLine();
        
        int randomDice = random.nextInt(4);
        for (int i=0; i<numberOfPlayer; i++){
            if (players[i].getName().equals(name)){
                System.out.printf("| %s got dice number %d |\n",name, randomDice+1);
                players[i].play(d[randomDice]);
                players[i].checkLose();
            }
        }
    
        for (int i=0; i<4-numberOfPlayer; i++){
            if (bots[i].getName().equals(name)){
                System.out.printf("| %s got dice number %d |\n",name, randomDice);
                bots[i].play(d[randomDice]);
                bots[i].checkLose();
            }
        }
        
    }

}
