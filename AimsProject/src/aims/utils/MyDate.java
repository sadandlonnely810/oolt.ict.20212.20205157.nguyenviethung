package aims.utils;

import java.util.HashMap;
import java.lang.String;


public class MyDate {
    private int day;
    private int month;
    private int year;
    private HashMap<String, Integer> monthDic = new HashMap<String, Integer>();
    {
        monthDic.put("January", 1);
        monthDic.put("February", 2);
        monthDic.put("March", 3);
        monthDic.put("April", 4);
        monthDic.put("May", 5);
        monthDic.put("June", 6);
        monthDic.put("July", 7);
        monthDic.put("August", 8);
        monthDic.put("September", 9);
        monthDic.put("October", 10);
        monthDic.put("November", 11);
        monthDic.put("December", 12);
    }

    private HashMap<String, Integer> dateDic = new HashMap<String, Integer>();
    {
        dateDic.put("first", 1);
        dateDic.put("second", 2);
        dateDic.put("third", 3);
        dateDic.put("fourth", 4);
        dateDic.put("fifth", 5);
        dateDic.put("sixth", 6);
        dateDic.put("seventh", 7);
        dateDic.put("eighth", 8);
        dateDic.put("ninth", 9);
        dateDic.put("tenth", 10);
        dateDic.put("eleventh", 11);
        dateDic.put("twelfth", 12);
        dateDic.put("thirteenth", 13);
        dateDic.put("fourteenth", 14);
        dateDic.put("fifteenth", 15);
        dateDic.put("sixteenth", 16);
        dateDic.put("seventeenth", 17);
        dateDic.put("eighteenth", 18);
        dateDic.put("nineteenth", 19);
        dateDic.put("twenty first", 21);
        dateDic.put("twentieth", 20);
        dateDic.put("twenty second", 22);
        dateDic.put("twenty third", 23);
        dateDic.put("twenty fourth", 24);
        dateDic.put("twenty fifth", 25);
        dateDic.put("twenty sixth", 26);
        dateDic.put("twenty seventh", 27);
        dateDic.put("twenty eighth", 28);
        dateDic.put("twenty ninth", 29);
        dateDic.put("thirtieth", 30);
        dateDic.put("thirty first", 31);
    }

    public void setDay(int day) {
        this.day = day;
    }
    public void setMonth(int month) {
        this.month = month;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public int getDay() {
        return day;
    }
    public int getMonth() {
        return month;
    }
    public int getYear() {
        return year;
    }

    public MyDate(){
        this.day = 13;
        this.month = 5;
        this.year = 2022;
    }

    public MyDate(int day, int month, int year){
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate(String date){
        HashMap<String, Integer> monthDic = new HashMap<String, Integer>();
        monthDic.put("January", 1);
        monthDic.put("February", 2);
        monthDic.put("March", 3);
        monthDic.put("April", 4);
        monthDic.put("May", 5);
        monthDic.put("June", 6);
        monthDic.put("July", 7);
        monthDic.put("August", 8);
        monthDic.put("September", 9);
        monthDic.put("October", 10);
        monthDic.put("November", 11);
        monthDic.put("December", 12);

        String[] splitString = date.split("\\s+");
        String monthString = splitString[0];
        String dayString = splitString[1].substring(0,2);
        String yearString = splitString[2];

        this.month = monthDic.get(monthString);
        this.day = Integer.parseInt(dayString);
        this.year = Integer.parseInt(yearString);
    }

    public MyDate(String day, String month, String year){

        
        this.day = dateDic.get(day);
        this.month = monthDic.get(month);
        this.year = 2022;
    }

    public void printDate1(){
        System.out.printf("The date: %d-%d-%d\n", this.year, this.month, this.day);
    }

    public void printDate2(){
        System.out.printf("The date: %d/%d/%d\n", this.day, this.month, this.year);
    }

    public void printDate3(){
        HashMap<Integer, String> month = new HashMap<>();
        month.put(1, "Jan");
        month.put(2, "Feb");
        month.put(3, "Mar");
        month.put(4, "Apr");
        month.put(5, "May");
        month.put(6, "Jun");
        month.put(7, "Jul");
        month.put(8, "Aug");
        month.put(9, "Sep");
        month.put(10, "Oct");
        month.put(12, "Nov");
        month.put(13, "Dec");

        System.out.printf("The date: %d-%3s-%d\n", this.day, month.get(this.month), this.year);
    }

}
