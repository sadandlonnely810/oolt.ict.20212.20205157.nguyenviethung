package aims.media;

import java.util.ArrayList;

public class CompactDisc extends Media implements Playable {
    private String artist;
    private ArrayList<Track> tracks = new ArrayList<>();
    private int cd_length = 0;

    
    public String getArtist() {
        return artist;
    }
    

    public int getCd_length() {
        return cd_length;
    }


    public CompactDisc(String title, String category, float cost, String artist, ArrayList<Track> tracks) {
        super(title, category, cost);
        this.artist = artist;
        this.tracks = tracks;
        for (Track track : tracks){
            this.cd_length+=track.getLength();
        }
    }

    public CompactDisc(String title, String category, float cost, String artist) {
        super(title, category, cost);
        this.artist = artist;
        for (Track track : tracks){
            this.cd_length+=track.getLength();
        }
    }

    @Override
    public void play() {
        // TODO Auto-generated method stub
        System.out.println("Playing CD "+this.getTitle());
        System.out.println("Length of CD: "+this.getCd_length());
        System.out.println("Artist: "+this.getArtist());
        for (Track track : tracks){
            track.play();
        }
    }

    
    
}
