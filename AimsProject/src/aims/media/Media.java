package aims.media;

import java.util.Random;

abstract class Media {
    private String title;
    private String category;
    private float cost;
    private int id;
    private Random random = new Random();


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }


    public String getCategory() {
        return category;
    }


    public float getCost() {
        return cost;
    }

    


    public Random getRandom() {
        return random;
    }


    public Media() {
    }


    public Media(String title, String category, float cost) {
        this.title = title;
        this.category = category;
        this.cost = cost;
        this.id = random.nextInt(100)+1;
    }


    public Media(String title, String category) {
        this.title = title;
        this.category = category;
    }


    public Media(String title) {
        this.title = title;
    }
    
}
