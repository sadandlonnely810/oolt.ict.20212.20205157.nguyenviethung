package aims.media;

import java.util.ArrayList;

public class Book extends Media {
    private ArrayList<String> authors = new ArrayList<>();
    
    

    public Book(String title, String category, float cost, ArrayList<String> authors) {
        super(title, category, cost);
        this.authors = authors;
    }

    public Book(String title, String category, float cost) {
        super(title, category, cost);
    }
   
    public Book(String title) {
        super(title);
    }

    public Book(String title, String category){
        super(title, category);
    }
    
    public Book() {
    }

    public void addAuthor(String author){
        if (this.authors.contains(author))
            System.out.printf("%s", "Author is already exist");
        else{
            this.authors.add(author);
            System.out.printf("%s", "Author is added successfully");
        }
    }

    public void removeAuthor(String author){
        if (this.authors.contains(author)){
            this.authors.remove(author);
            System.out.printf("%s", "Author is removed successfully");
        }
        else{
            System.out.printf("%s", "Author is not exist");
        }
    }

    
}
