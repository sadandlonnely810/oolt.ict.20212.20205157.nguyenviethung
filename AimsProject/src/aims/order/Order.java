package aims.order;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import aims.media.CompactDisc;
import aims.media.DigitalVideoDisc;
import aims.utils.MyDate;

public class Order {
    public static final int  MAX_ORDER = 5;

    private ArrayList<Object> itemOrder = new ArrayList<>();

    private int qtyOrder = itemOrder.size();

    private MyDate dateOrdered;

    private static int numberOrdered = 0;


    public Order(){
        if (Order.numberOrdered+1 > MAX_ORDER)
            JOptionPane.showMessageDialog(null, "You have reached the limit order times (5)");
        else{
            Order.numberOrdered++;
            this.dateOrdered = new MyDate("January 31st 2022");
        }
            
    }

    public void getQtyOrdered(){
        JOptionPane.showMessageDialog(null, "The number of order dvd: "+qtyOrder);
    }

    public void addMedia(Object med){
        itemOrder.add(med);
        qtyOrder++;
        System.out.printf("%s", "Add successfully\n");
    }

    public void removeMedia(Object med){
        if(qtyOrder <= 0)
            System.out.printf("%s", "The ordered list is empty\n");
        else{
            if (itemOrder.remove(med)){
                System.out.printf("%s", "Remove successfully\n");  
                qtyOrder--;
            }
            else
                System.out.printf("%s", "The media does not exist in ordered list\n");
        }
    }

    public void getMedia(){
        for (Object oj : itemOrder){
            if (oj.getClass() == DigitalVideoDisc.class){
                DigitalVideoDisc dvd = (DigitalVideoDisc) oj;
                dvd.play();
            }
            else if (oj.getClass() == CompactDisc.class){
                CompactDisc cd = (CompactDisc) oj;
                cd.play();
            }   
        }
    }

    public void clearOrder(){
        if (Order.numberOrdered+1 > 5){
            JOptionPane.showMessageDialog(null, "You have reached the limit order times (5)");
            return;
        }
        JOptionPane.showMessageDialog(null, "Create new order successfully");
        this.itemOrder.clear();
        Order.numberOrdered++;
        
    }
        
}


