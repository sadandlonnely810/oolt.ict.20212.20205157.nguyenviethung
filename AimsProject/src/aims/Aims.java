package aims;

import java.util.ArrayList;
import java.util.Scanner;

import aims.media.Book;
import aims.media.DigitalVideoDisc;
import aims.order.Order;
import aims.media.CompactDisc;
import aims.media.Track;

public class Aims {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        Order order = new Order();
        
        int choice;
        

        do {
            System.out.println("Order Management Application: ");
            System.out.println("--------------------------------");
            System.out.println("1. Create new order");
            System.out.println("2. Add item to the order");
            System.out.println("3. Play all dvd and cd");
            System.out.println("4. Display the items list of order");
            System.out.println("0. Exit");
            System.out.println("--------------------------------");
            System.out.println("Please choose a number: 0-1-2-3-4");
            choice = scanner.nextInt();
            scanner.nextLine();

            switch (choice) {
                case 1:
                    order.clearOrder();
                    break;
                case 2:
                    order = addItem(order);
                    break;
                case 3:
                    order.getMedia();
                    break;
                case 4:
                    
                    break;
                case 0:
                    System.out.println("Thanks for using my service");
                    break;
            }
        } while (choice != 0);
 
    }

    public static Order addItem(Order order){
        int choice;
        Scanner scanner = new Scanner(System.in);

        String title, category;
        float cost;

        String artist;
        int length;
        
        
        do {
            System.out.println("0. Exit");
            System.out.println("1. Add DVD");
            System.out.println("2. Add book");
            System.out.println("3. Add compact disc");
            System.out.println("Please choose a number: ");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 1:
                    System.out.println("Enter title of DVD");
                    title = scanner.nextLine();
                    System.out.println("Enter category of DVD");
                    category = scanner.nextLine();
                    System.out.println("Enter cost of DVD");
                    cost = scanner.nextFloat();
                    scanner.nextLine();
                    System.out.println("Enter director of DVD");
                    artist = scanner.nextLine();
                    System.out.println("Enter length of DVD");
                    length = scanner.nextInt();
                    scanner.nextLine();

                    DigitalVideoDisc dvd = new DigitalVideoDisc(title, category, cost, length, artist);
                    order.addMedia(dvd);
                    break;
                case 2:
                    System.out.println("Enter title of book");
                    title = scanner.nextLine();
                    System.out.println("Enter category of book");
                    category = scanner.nextLine();
                    System.out.println("Enter cost of book");
                    cost = scanner.nextFloat();
                    scanner.nextLine();

                    Book book = new Book(title, category, cost);
                    order.addMedia(book);
                    break;
                case 3:
                    ArrayList<Track> tracks = new ArrayList<>();
                    int choice_CD;

                    System.out.println("Enter title of CD");
                    title = scanner.nextLine();
                    System.out.println("Enter category of CD");
                    category = scanner.nextLine();
                    System.out.println("Enter cost of CD");
                    cost = scanner.nextFloat();
                    scanner.nextLine();
                    System.out.println("Enter artist of CD");
                    artist = scanner.nextLine();
                    
                    do {
                        System.out.println("Do you want to add track to CD");
                        String track_title;
                        int track_length;
                        System.out.println("1. YES");
                        System.out.println("2. NO");
                        choice_CD = scanner.nextInt();
                        scanner.nextLine();
                        switch (choice_CD) {
                            case 1:
                                System.out.println("Enter title of track");
                                track_title = scanner.nextLine();
                                System.out.println("Enter length of track");
                                track_length = scanner.nextInt();
                                scanner.nextLine();
                                Track track = new Track(track_title, track_length);
                                tracks.add(track);
                                break;
                        
                            default:
                                break;
                            
                        }
                    } while (choice_CD != 2);
                    CompactDisc cd = new CompactDisc(title, category, cost, artist, tracks);
                    order.addMedia(cd);
                    break;


                default:
                    break;
            } 
        } while (choice != 0);
        return order;
    } 
}

