package test;

import aims.utils.MyDate;

public class DateTest {
    public static void main(String[] args){


        MyDate date1 = new MyDate(12,10,2022);
        MyDate date2 = new MyDate("January 31st 2024");
        MyDate date3 = new MyDate();
        MyDate date4 = new MyDate("thirteenth", "February", "twenty twenty-two");

        date1.printDate1();
        date2.printDate2();
        date3.printDate3();
        date4.printDate3();
    }
}
