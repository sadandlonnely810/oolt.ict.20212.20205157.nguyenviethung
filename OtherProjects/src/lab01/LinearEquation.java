package lab01;

import javax.swing.JOptionPane;

public class LinearEquation {
    public static void main(String[] args){
        float a, b, x;
        do{
            a = Float.parseFloat(JOptionPane.showInputDialog("Enter a: "));
        } while (a==0);
        b = Float.parseFloat(JOptionPane.showInputDialog("Enter b: "));
        x = - (b / a);

        JOptionPane.showMessageDialog(null, "The variable x: "+x);
        
    }

}
