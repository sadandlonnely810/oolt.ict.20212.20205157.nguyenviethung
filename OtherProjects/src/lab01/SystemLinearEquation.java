package lab01;

import javax.swing.JOptionPane;

public class SystemLinearEquation {
    public static void main(String[] args){
        float[][] coeff = new float[2][3];
        for (int i=0; i<2; i++){
            for (int j = 0; j<3; j++){
                coeff[i][j] = Float.parseFloat(JOptionPane.showInputDialog("Enter a" + (i+1) + (j+1) +": "));
            }
        }
        float[] deter = new float[3];
        deter[0]=coeff[0][1]*coeff[1][1] - coeff[1][0]*coeff[0][1];
        deter[1]=coeff[0][2]*coeff[1][1] - coeff[1][2]*coeff[0][1];
        deter[3]=coeff[0][0]*coeff[1][2] - coeff[1][0]*coeff[0][2];
        float x1,x2;

        if (deter[0] != 0){
            x1 = deter[1]/deter[0];
            x2 = deter[2]/deter[0];
            JOptionPane.showMessageDialog(null, "The variable x1: "+x1);
            JOptionPane.showMessageDialog(null, "The variable x2: "+x2);
        } else if (deter[0]==0 && deter[1]==0 && deter[2]==0){
            JOptionPane.showMessageDialog(null, "Infinitive solutions");
        } else{
            JOptionPane.showMessageDialog(null, "No solution");
        }
    }
}
