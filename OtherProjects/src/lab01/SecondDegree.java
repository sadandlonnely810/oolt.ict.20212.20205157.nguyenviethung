package lab01;

import javax.swing.JOptionPane;

public class SecondDegree {
    public static void main(String[] args){
        double[] coeff = new double[3];
        for (int i=0; i<3; i++){
            coeff[i] = Double.parseDouble(JOptionPane.showInputDialog("Enter coefficient: "));
        }
        
        double delta = Math.pow(coeff[1], 2) - 4*coeff[0]*coeff[2];
        double x1,x2;

        if (delta == 0){
            double temp = Math.sqrt(delta);
            x1 = (-coeff[1] + temp) / 2*coeff[0];
            x2 = (-coeff[1] - temp) / 2*coeff[0];
            JOptionPane.showMessageDialog(null, "The double root: "+ x1);
        } else if (delta > 0){
            double temp = Math.sqrt(delta);
            x1 = (-coeff[1] + temp) / 2*coeff[0];
            x2 = (-coeff[1] - temp) / 2*coeff[0];
            JOptionPane.showMessageDialog(null, "X1: "+ x1);
            JOptionPane.showMessageDialog(null, "X2: "+ x2);
        } else {
            JOptionPane.showMessageDialog(null, "No solution");
        }
        System.exit(0);
    }
}
