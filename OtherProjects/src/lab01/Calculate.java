package lab01;

import javax.swing.JOptionPane;

public class Calculate {
    public static void main(String[] args){
        float number1 = Float.parseFloat(JOptionPane.showInputDialog("Enter the first float number"));

        float number2 = Float.parseFloat(JOptionPane.showInputDialog("Enter the second float number"));

        float sum, difference, product, quotient;
        sum = number1 + number2;
        difference = Math.abs(number1-number2);
        product = number1*number2;
        quotient = number1 / number2;

        JOptionPane.showMessageDialog(null, "SUM: " + sum);
        JOptionPane.showMessageDialog(null, "DIFFERENCE: " + difference);
        JOptionPane.showMessageDialog(null, "PRODUCT: " + product);
        JOptionPane.showMessageDialog(null, "QUOTIENT: " + quotient);
        System.exit(0);
    }
}
